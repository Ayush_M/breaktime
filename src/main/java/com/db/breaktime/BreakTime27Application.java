package com.db.breaktime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BreakTime27Application {

	public static void main(String[] args) {
		SpringApplication.run(BreakTime27Application.class, args);
	}

}
