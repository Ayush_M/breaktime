package com.db.breaktime.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

//calendarId INT PRIMARY KEY, emailID varchar(50), freeTime TIMESTAMP, duration varchar(10),
@Entity
@Table(name = "Calendar")
public class Calendar {
	
	@Id
	int calendarId;
	@Column(name = "freeTime" )
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	Timestamp freeTime;
	@Column(name = "duration" )
	String duration;
	@Column(name = "emailID")
	String emailID;
	
	public int getCalendarId() {
		return calendarId;
	}
	public void setCalendarId(int calendarId) {
		this.calendarId = calendarId;
	}
	public Timestamp getFreeTime() {
		return freeTime;
	}
	public void setFreeTime(Timestamp freeTime) {
		this.freeTime = freeTime;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getEmailID() {
		return emailID;
	}
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
}

