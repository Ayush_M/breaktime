package com.db.breaktime.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Users")
public class Users {
	
	@Id
	String emailID;
	
	@Column(name = "name")
	private String name;

	@Column(name = "snooze")
	private int snooze;
	
	@OneToMany(mappedBy = "Users")
	
	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSnooze() {
		return snooze;
	}

	public void setSnooze(int snooze) {
		this.snooze = snooze;
	}

}
