package com.db.breaktime.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*
User_Interest_Map(emailID varchar(50) , interestId int, 
		PRIMARY KEY(emailID, interestId),
	    FOREIGN KEY(emailID) REFERENCES User(emailID),
	    FOREIGN KEY(interestId) REFERENCES Interests(interestId)
	    );
*/
@Entity
@Table(name = "UserInterestMap")
public class UserInterestMap {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int mapID;
	@Column(name = "emailId")
	String emailID;
	@Column(name = "interestId")
	int interestId;
	
	
	public int getInterestId() {
		return interestId;
	}
	public void setInterestId(int interestId) {
		this.interestId = interestId;
	}
	public int getMapID() {
		return mapID;
	}
	public void setMapID(int mapID) {
		this.mapID = mapID;
	}
	public String getEmailID() {
		return emailID;
	}
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	
	
}
