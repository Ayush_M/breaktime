package com.db.breaktime.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Interests")
public class Interests {
	
	@Id
	@Column(name="interestId")
	int interestId;
	@Column(name  = "interestName")
	String interestName;
	@Column(name = "category")
	String category;
	
	@OneToMany(mappedBy = "Interests")
	public int getInterestId() {
		return interestId;
	}
	public void setInterestId(int interestId) {
		this.interestId = interestId;
	}
	public String getInterestName() {
		return interestName;
	}
	public void setInterestName(String interestName) {
		this.interestName = interestName;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	
}
