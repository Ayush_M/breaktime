package com.db.breaktime.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Content")
public class Content {
	//cid,content,interestId)
	@Id
	int cId;
	@Column(name = "content")
	String content;
	
	/*@ManyToOne
	@JoinColumn({
		@JoinColumn(name = "interestId"),
		@JoinColumn(name = "codRegion", insertable = false, updatable = false)
	})*/
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "interestId", insertable = false, updatable = false)
    private Interests interest;

	public int getcId() {
		return cId;
	}
	public void setcId(int cId) {
		this.cId = cId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	
	
}
