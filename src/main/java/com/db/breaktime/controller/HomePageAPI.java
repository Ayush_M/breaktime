package com.db.breaktime.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.db.breaktime.dao.UsersDAO;
import com.db.breaktime.models.Users;

@RestController
@RequestMapping(value = "/")
public class HomePageAPI {
	
	
	@Autowired
	private UsersDAO userDAO;
	

	private boolean unsuccessful = false;
	private boolean successful = true;

	@PostMapping
	public @ResponseBody boolean createUser(@RequestBody Users user) {
	
		try {
			userDAO.save(user);
		}
		catch(Exception e)
		{
			return unsuccessful;
		}
		return successful;
	}


}
