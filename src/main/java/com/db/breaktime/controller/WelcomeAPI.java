package com.db.breaktime.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.db.breaktime.dao.UserInterestMapDAO;
import com.db.breaktime.models.UserInterestMap;
import com.db.breaktime.queries.CalendarRepository;
import com.db.breaktime.queries.InterestsRepository;
import com.db.breaktime.queries.UserInterestMapRepository;
import com.db.breaktime.queries.UsersRepository;
@RestController
@RequestMapping(value = "welcome")
public class WelcomeAPI {
	
	@Autowired
	private InterestsRepository dCategory;
	
	@Autowired
	private UserInterestMapRepository uIMapR;
	
	@Autowired
	private UsersRepository usersR;
	
	@Autowired
	private CalendarRepository calendarR;
	
	@Autowired
	private UserInterestMapDAO userInterestMapDAO;

	private boolean unsuccessful = false;
	private boolean successful = true;
	
	@GetMapping
	public @ResponseBody List<String> fetchCategories() {
		List<String> categories = new ArrayList<String>();
		try 
		{
			categories = dCategory.findDistinctCategory();
		}
		catch(Exception e)
		{
			System.out.println(e); 
		}
		return categories;
	}
	
	@GetMapping(value = "/{category}")
	public @ResponseBody List<String> fetchInterestNames(@PathVariable String category ) {
		List<String> interests = new ArrayList<String>();
		System.out.println("called.");
		try 
		{
			interests = dCategory.findInterestNames(category);
			System.out.println(interests);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return interests;
	}

	
	@DeleteMapping(value = "/deleteAccount/{email}")
	public @ResponseBody boolean deleteUserAccount(@PathVariable String email) {
		System.out.println("called.");
		try 
		{
			//Delete from userinterestmap
			int s = uIMapR.deleteFromUserInterestMap(email);
			if(s == 1)
				System.out.println("Entries deleted successfully from UserInterestMap table");
			else
				System.out.println("Could not delete from UserInterestMap");
			 
			//delete from calendar
			s = calendarR.deleteUserFromCalendar(email);
			if(s == 1)
				 System.out.println("Entries deleted successfully from Calendar");
			else
				 System.out.println("Could not delete from Calendar table");
				 
			//delete from users
			s = usersR.deleteAccountfromUsers(email);
			if(s == 1)
				System.out.println("Entries deleted successfully from Users table");
			else
				System.out.println("Could not delete from Users table");
		}
			catch(Exception e)
			{
				System.out.println(e);
				return unsuccessful;
			}
			
			return successful;
		}

	@PutMapping(value = "/snooze/{email}")
	public @ResponseBody boolean snooze(@PathVariable String email) {
		
		//Retrieve from file
		System.out.println("called.");
		try 
		{
			int status = usersR.snooze(email);
			if(status==1)
				System.out.println("Snooze set ON");
			else
				System.out.println("Could not set snooze ON");
		}
		catch(Exception e)
		{
			System.out.println(e);
			return unsuccessful;
		}
		
		return successful;
	}

	@PutMapping(value = "/unsnooze/{email}")
	public @ResponseBody boolean unsnooze(@PathVariable String email) {
		System.out.println("called.");
		try 
		{
			int status = usersR.unsnooze(email);
			if(status==1)
				System.out.println("Snooze set OFF");
			else
				System.out.println("Could not set snooze  OFF");
		}
		catch(Exception e)
		{
			System.out.println(e);
			return unsuccessful;
		}
		
		return successful;
	}

	@GetMapping(value = "/userName/{email}")
	public @ResponseBody String fetchUsername(@PathVariable String email) {
			System.out.println("called.");
			String name = "";
			try 
			{
				 name = name + usersR.fetchUserName(email);
			}
			catch(Exception e)
			{
				System.out.println(e);
				return null;
			}
			
			return name;
		}
	
	@PutMapping(value = "/addInterest/{email}/{category}/{subcategory}")
	public boolean addInterest(@PathVariable String email, @PathVariable String category, @PathVariable String subcategory) {  
		try 
		{
			//Get interest id for given category and sub-category
			int interestID= dCategory.findInterestId(subcategory, category);
			System.out.println(interestID);
		    
		    //create userInterestMap object and set information.
		    UserInterestMap uim = new UserInterestMap();
		    uim.setInterestId(interestID);
		    uim.setEmailID(email);
			//Save mapping in userInterestMap Table 
			userInterestMapDAO.save(uim);
		}
		catch(Exception e)
		{
			return unsuccessful;
		}
		return successful;  
	    }
	
	@DeleteMapping(value = "/deleteInterest")
	public boolean deleteInterest() {  
		//Retrieve Data FROM ONE TABLE  
		try 
		{
			//Get interest id for given category and sub-category
			int interestID= dCategory.findInterestId("Country Music", "Music");
			System.out.println(interestID);
			
			//Delete row with given interestID and emailId from UserInterestMap.
			int status = uIMapR.deleteInterest("test@gmail.com", interestID);
			if(status == 0)
				System.out.println("Could not delete interest from UserInterestMap table.");
			else
				System.out.println("Deleted interest from UserInterestMap table.");
		}
		catch(Exception e)
		{
			return unsuccessful;
		}
		return successful;  
	}

}
