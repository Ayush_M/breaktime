package com.db.breaktime.queries;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.db.breaktime.models.Calendar;

public interface CalendarRepository extends CrudRepository<Calendar, Integer> {
	@Transactional
	@Modifying
	@Query(value="delete from Calendar c where c.emailID = :email", nativeQuery = true)
	int deleteUserFromCalendar(@Param("email") String email);
	
	@Query(value="SELECT * FROM Calendar", nativeQuery = true)
	List<String> fetchCalendar();
}

