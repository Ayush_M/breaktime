package com.db.breaktime.queries;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.db.breaktime.models.Interests;

public interface InterestsRepository extends CrudRepository<Interests, Integer> {
	@Query("SELECT DISTINCT a.category FROM Interests a")
    List<String> findDistinctCategory();
	

	@Query("SELECT  a.interestName FROM Interests a where a.category = :category") 
	List<String> findInterestNames(@Param("category") String category);
	
	@Query("SELECT interestId from Interests WHERE interestName = :interestName AND category = :category")
	int findInterestId(@Param("interestName") String interestName, @Param("category") String category);
	
	
}
