package com.db.breaktime.queries;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.db.breaktime.models.Users;

public interface UsersRepository extends CrudRepository<Users, String> {
	@Transactional
	@Modifying
	@Query("delete from Users x where x.emailID=:email")
    int deleteAccountfromUsers(@Param("email") String email);
	
	@Transactional
	@Modifying
	@Query("update Users u set u.snooze = 1 where u.emailID = :email")
    int snooze(@Param("email") String email);
	
	@Transactional
	@Modifying
	@Query("update Users u set u.snooze = 0 where u.emailID = :email")
    int unsnooze(@Param("email") String email);
	
	@Query("SELECT u.name FROM Users u WHERE u.emailID=:email")
	String fetchUserName(@Param("email") String email);
}
