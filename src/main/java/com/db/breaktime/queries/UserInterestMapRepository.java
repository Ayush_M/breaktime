package com.db.breaktime.queries;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.db.breaktime.models.UserInterestMap;

public interface UserInterestMapRepository extends CrudRepository<UserInterestMap, Integer>{
	
		@Transactional
		@Modifying
	 	@Query("delete from UserInterestMap m where m.emailID= :email")
	    int deleteFromUserInterestMap(@Param("email") String email);
		
		@Transactional
		@Modifying
	 	@Query("delete from UserInterestMap m where m.emailID= :email AND m.interestId = :interestID")
	    int deleteInterest(@Param("email") String email, @Param("interestID") int interestID);		

}

//public interface deleteAccFromUserInterestMap extends CrudRepository<UserInterestMap, Integer> {
//	 @Query("delete from UserInterestMap m where m.emailId='test@gmail.com'")
//	    deleteFromUserInterestMap();
//
//}