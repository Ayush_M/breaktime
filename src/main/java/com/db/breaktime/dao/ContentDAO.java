package com.db.breaktime.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import com.db.breaktime.models.Content;
public interface ContentDAO  extends JpaRepository<Content, Integer> {

}

