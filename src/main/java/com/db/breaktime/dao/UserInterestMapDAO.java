package com.db.breaktime.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.db.breaktime.models.UserInterestMap;

public interface UserInterestMapDAO extends JpaRepository<UserInterestMap, Integer> {

}