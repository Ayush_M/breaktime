package com.db.breaktime.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.db.breaktime.models.Calendar;


public interface CalendarDAO extends JpaRepository<Calendar, Integer> {

}
