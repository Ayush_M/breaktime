package com.db.breaktime.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import com.db.breaktime.models.MiniTasks;

public interface MiniTasksDAO extends  JpaRepository<MiniTasks, Integer> {

}
