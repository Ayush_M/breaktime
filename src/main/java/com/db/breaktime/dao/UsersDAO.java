package com.db.breaktime.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.db.breaktime.models.Users;

public interface UsersDAO extends JpaRepository<Users, String> {

}
